OnOff
=====

OnOff provides a simple web-based on and off switch for the raspberrypi.
It does not offer any protection against abuse, you need to add
authentication via your webserver configuration or only run this in a safe
environment.

License
-------
OnOff is licensed under the GPL v3, the fully license text is included
in the file COPYING.
