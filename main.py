# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import RPi.GPIO as GPIO
#setup GPIO using Board numbering
GPIO.setmode(GPIO.BOARD)
# Don't warn that we have previously set the channel
GPIO.setwarnings(False)
from functools import wraps
from flask import Flask
from flask import request, Response, redirect, render_template, url_for
app = Flask(__name__)

def get_state():
    GPIO.setup(7, GPIO.OUT)
    state = GPIO.input(7)
    return state

def set_state(state):
    GPIO.setup(7, GPIO.OUT)
    GPIO.output(7, state)

def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    return username == 'ad' and password == 'sekret'

def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
    'Could not verify your access level for that URL.\n'
    'You have to login with proper credentials', 401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'})

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated

@app.route("/")
@requires_auth
def root():
    state = get_state()
    return render_template('state.html', state=state)
    # we don't just redirect as below, because that actually switches something
    #return redirect(url_for('turn_onoff', state=state))

@app.route('/state/')
@app.route("/state/<int:state>")
@requires_auth
def turn_onoff(state=0):
    set_state(state)
    return render_template('state.html', state=state)


if __name__ == "__main__":
    app.run(debug=False)
